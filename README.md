### rd-homepage
A simple repo that deploy index.zip as your server code
along with the ssl cert and nginx config file as a docker image to your target server
### Note that this is a public repo.
not to put sensitive files on the repo

or .gitignore the files before commit push
# To deploy to your server
### 1. add your pub key to servers' authorized_keys
```
~/.ssh/authorized_keys
```
### 2. SERVER_IP as env var
export SERVER_IP=YOUR_SERVER_IP

### 3. prepare ssl cert
```
cp path/to/server.cert rd/server.cert
cp path/to/server.key rd/server.key
```
### 4. put the index.zip with the index.html and the assets file
```
cp path/to/index.zip rd/index.zip
```

### 5. tar and scp the code and config to the server
```
tar --exclude='./*/.git' -zcvf rd.tgz ./rd
scp rd.tgz root@$SERVER_IP:/srv/
```

### 6. remote execute bootstrap script to install packages
```
ssh -i ~/.ssh/id_rsa root@$SERVER_IP 'bash -s' < deploy/bootstrap.sh
```

### 7. remote execute deploy script to deploy the docker image
```
ssh -i ~/.ssh/id_rsa root@$SERVER_IP 'bash -s' < deploy/deploy.sh
```
