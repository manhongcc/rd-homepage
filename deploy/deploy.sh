set -x

tar -xvzf /srv/rd.tgz -C /srv
cd /srv/rd
make build
make stop
make rm
make run

echo `date` success >> /tmp/deploy.log
